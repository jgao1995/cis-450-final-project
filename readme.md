# CIS 450 Final Project

* Authors: Joseph Gao, Yuqing (Carrie) Wang, Karthic, Thomas Lee
* Topic: housing guides for top 10 U.S. cities. Housing (income): zillow house median values; neighborhoods, weather, yelp. Based on your income level, neighborhood preferences, weather preferences, restaurant preferences, gives a score on the neighborhood you want to move to from 0 to 10.

# Description
This app is a city recommendation service that aims to help users better choose their next city based on environment variables that are important to them. We provide our users with 7 preset queries that they are able to modify and use, with each query aiming to satisfy a different set of environment variables that the user may find important to themselves. For example, users may be interested in finding out which city they should check out if they want to live in a city that has an average temperature of 70 degrees, or they might be interested in finding a city that has a lot of pediatricians. For each of those unique scenarios, our team gathered data from surveys and pinpointed the top 7 questions individuals might be asking themselves when searching for a new city. Our list of queries is by no means comprehensive, but our hope is that we will be able to provide some insightful recommendations using the skills we have learned from our CIS 450 class. We chose ten cities to use as potential recommendations. We limited our domain to the 10 major cities of the United States because the custon Yelp API wrapper we wrote only allowed us to make a query for 25 results from a single city. Our wrapper bypassed the 25 results barrier by making multiple requests (20, to be exact) so we would have 500 Yelp results for a single city, and doing this for 10 cities ultimately exhausted our Yelp API usage in the allowable timeframe, and thus we made the executive decision to just stick with 10 cities with 500 yelp results each. As a result, we had to throw out the weather data and zillow data associated with cities that weren't in the top 10 US cities. 

# Example Questions a User Might Have:
1. What is the most affordable city given my current income with an average temperature of 70 deg?
2. What is the most expensive neighborhood in this city with the best local chinese food? 
3. What neighborhood should I live in if I'm on a budget but really enjoy being dry? (constrained on being dry, find city, then find cheap neighborhood)
4. What city should I move to if I want to get the most bang for my buck for food?
5. Which neighborhood in x city has the highest density of medical support services?
6. Where should I live if I never want to be bored but am strapped for cash?
7. Where should I live if I am financially well off, and wish to have a lot of fun in the sun? 
8. I want to be able to compare the climates of two cities side by side using a graph before making a decision.

# Technology:
1. Amazon AWS
2. Git + BitBucket 
3. Flask
4. Yelp Api for restaurant data
5. Data links: zillow: http://www.zillow.com/research/data/; weather: https://www.wunderground.com/weather/api/?MR=1; yelp: we are using API to scrap our own data

# Screnshots 

## Main landing page
![](./ss1.png)

## Query page
![](./ss2.png)

## Example query results page
![](./ss3.png)

## Climate Comparison page
![](./ss4.png)

## Example climate comparison graph page
* The graph is generated in real time by querying our DB and then building the graph! 
![](./ss5.png)

# Environment Setup
Our project is a Flask python application that uses Oracle and MongoDB as the backend database conncetions. In order to run this app locally, you **MUST have python installed, and you MUST also have the cx_Oracle instantclient package installed.** Python for obvious reasons, and the oracle instantclient because there is NO OTHER WAY to connect to an oracle database through a Python webapp in an easy and concise manner. I have done a lot of reserach, and found that this is the best way.
1. Python installation: `https://www.python.org/`
2. cx_Oracle instant client: `http://www.oracle.com/technetwork/database/features/instant-client/index.html`
3. Helpful links for cx_Oracle instant client trouble: `https://gist.github.com/kimus/10012910`

# Project Setup
Now that you have the environment setup correctly, we must go through the project setup. 
1. Clone this repository
2. Navigate to the root directory
```python
pip install virtualenv
virtualenv ENV
source ENV/bin/activate
pip install -r requirements.txt
python run.py
```
3. Navigate on your browser to localhost:5000 and you should see the webapp running!
4. NOTE: If you cannot get this project running locally, please let someone in the team know, and we will be happy to either do a in person demo for you or help you set it up. We spoke with Shreya about potential setup issues, and she said if it the TA isn't able to get the project running, they will just read our code and make sure it lines up with what we presented. I have included screenshots (see above) to help with the grading, and if I somehow manage to get the project hosted online, I will send an email out to Shreya with a link to the project online!