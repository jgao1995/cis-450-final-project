from app import app
from flaskext.mysql import MySQL
from flask import render_template
from flask import request
import json
import cx_Oracle
from flask.ext.pymongo import PyMongo
import pymongo
import pdb
from bson.code import Code


# DB setup
mysql = MySQL()
ip = 'cis450project.cl4wvimcqrp7.us-west-2.rds.amazonaws.com'
port = 1521
sid = 'PENNDB'
dsn_tns = cx_Oracle.makedsn(ip, port, sid)
app.config['MYSQL_DATABASE_USER'] = 'cis450_mysql'
app.config['MYSQL_DATABASE_PASSWORD'] = 'cis450_mysql'
app.config['MYSQL_DATABASE_DB'] = 'cis450_mysql'
app.config['MYSQL_DATABASE_HOST'] = 'cis450-mysql.cl4wvimcqrp7.us-west-2.rds.amazonaws.com'
app.config['MYSQL_PORT']=3306
mysql.init_app(app)

app.config['MONGO1_HOST'] = 'ds161630.mlab.com'
app.config['MONGO1_PORT'] = 61630
app.config['MONGO1_DBNAME'] = 'cis450project'
app.config['MONGO1_USERNAME'] = 'user'
app.config['MONGO1_PASSWORD'] = 'password'
mysql.init_app(app)
mongo = PyMongo(app, config_prefix='MONGO1')
# end of DB setup

data = {}

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/queries")
def queries():
    return render_template('query.html')

@app.route("/compare")
def compare():
    return render_template('compare.html')

@app.route("/do_query_1")
def query_1():
    temp = float(request.args.get("temp"))
    temp = (temp-32.0) * (5.0/9.0)
    query = """select city_name
            from zillow
            where price = 
            (select min(price) 
            from zillow z inner join highc c on z.city_name = c.city_name where c.val_sum > """+str(temp)+"*12)"
    db = cx_Oracle.connect('cis450project', 'cis450project', dsn_tns)
    cursor = db.cursor()
    cursor.execute(query)
    response = cursor.fetchall()
    if (response):
        data['city'] = response[0][0]
        return render_template('city.html', city=data['city'], state=find_state(data['city']), text="Hello World.")
    else:
        return render_template('not_found.html')

@app.route("/do_query_2")
def query_2():
    DOCTOR_TYPE = request.args.get('food')
    mapper = Code(""" function() {
        emit(this.city, {id: this.id, reviews: this.review_count, rating: this.rating});
        }
        """)
    reducer = Code (""" 
        function(key, values) {
            var reduced = {id: "", reviews: 0, rating: 0}
            values.forEach(function(value){
                var vrat = value.rating;
                var vrev = value.reviews;
                var rrat = reduced.rating;
                var rrev = reduced.reviews;
                if (vrat > rrat || (vrat=rrat && vrev>rrev)) {
                    reduced.id = value.id;
                    reduced.reviews = value.reviews;
                    reduced.rating = value.rating;
                }
            });
            return reduced;
        }
        """ )

    query = """ SELECT city_name 
                FROM Zillow
                GROUP BY city_name
            ORDER BY avg(price) DESC"""
    db = cx_Oracle.connect('cis450project', 'cis450project', dsn_tns)
    cursor = db.cursor()
    cursor.execute(query)
    response = cursor.fetchall()
    city_val = {tup[0]: 10-i for (i, tup) in enumerate(response)}
    test = mongo.db.yelp.map_reduce(mapper, reducer, "results", query={"top_category": DOCTOR_TYPE}).find()
    for t in test:
        review_val = t['value']['rating'] * t['value']['reviews']
        city_id = t['_id']
        if (city_id == 'Brooklyn'):
            city_id = 'New York'
        city_val[city_id] += review_val
    sorted_val = sorted(city_val, key=city_val.get, reverse=True)
    final_city=sorted_val[0]
    return render_template('city.html', city=final_city, state=find_state(final_city), text=find_state_data(final_city))


@app.route("/do_query_3")
def query_3():
    climate_type = 'min'
    budget_type = 'min'
    if request.args.get('budgetRadios') == 'high':
        budget_type = 'max'
    if request.args.get('climateRadios') == 'wet':
        climate_type = 'max'
    query = """SELECT H.city_name
        FROM Zillow H INNER JOIN Precip R on (H.city_name = R.city_name OR H.county_name = R.city_name OR
        H.metro_name = R.city_name) WHERE (R.val_sum) <= (SELECT """+climate_type+"""(val_sum) FROM Precip)
        AND R.city_name = H.city_name
        AND H.price = (SELECT """+budget_type+"""(Z.price)
        FROM Zillow Z
        WHERE Z.city_name = H.city_name)"""
    db = cx_Oracle.connect('cis450project', 'cis450project', dsn_tns)
    cursor = db.cursor()
    cursor.execute(query)
    reponse = cursor.fetchall()
    if (reponse):
        data['city'] = reponse[0][0]
        return render_template('city.html', city=data['city'], state=find_state(data['city']), text=find_state_data(data['city']))

# This finds the city with the highest density of the specified type of doctor (or business)
@app.route("/do_query_4")
def mongodb():
    #test = mongo.db.yelp.find_one_or_404({'top_category': 'burgers'})
    # return request.args.get('density')
    DOCTOR_TYPE = request.args.get('density') #Can be changed to: earnosethroat, pediatricians, internalmed, etc...
    mapper = Code("""
        function() {
        emit(this.postal_code, {count: 1, postal_code: this.postal_code, city: this.city});
        }
        """)
    reducer = Code("""
        function(key, values) {
            var reduced = {count: 0, postal_code: 0, city: 0}
            values.forEach(function(value){
                reduced.count = reduced.count + value.count;
                reduced.postal_code = value.postal_code;
                reduced.city = value.city;
            });
            return reduced;
        }
        """)
    test = mongo.db.yelp.map_reduce(mapper, reducer, "results", query={"top_category": DOCTOR_TYPE}).find()
    count = 0
    postal = 0
    city = 0
    for t in test:
        c = t['value']['count']
        if c > count:
            count = c
            postal = t['value']['postal_code']
            city = t['value']['city']
    return render_template('city.html', city=city, state=find_state(city), text=find_state_data(city))

@app.route("/do_query_5")
def query_5():
    income = request.args.get('income')
    delta = 0
    if income == 'low':
        delta = 20
    elif income == 'average':
        delta = 100
    else:
        delta = 300
    query = """SELECT H.city_name
            FROM Zillow H
            WHERE H.price > (SELECT avg(price)-"""+str(delta)+""" FROM Zillow) AND
            H.price < (SELECT avg(price)+"""+str(delta)+""" FROM Zillow)"""
    db = cx_Oracle.connect('cis450project', 'cis450project', dsn_tns)
    cursor = db.cursor()
    cursor.execute(query)
    reponse = cursor.fetchall()
    if (reponse):
        data['city'] = reponse[0][0]
        return render_template('city.html', city=data['city'], state=find_state(data['city']), text=find_state_data(data['city']))

@app.route("/do_query_6")
def query_6():
    weather = request.args.get('weather')
    contraint = 'MAX'
    if weather == 'dry':
        contraint = 'MIN'
    query = """SELECT P.city_name
            FROM Precip P
            WHERE P.val_sum = (SELECT """+contraint+"""(Pr.val_sum) FROM Precip Pr)"""
    db = cx_Oracle.connect('cis450project', 'cis450project', dsn_tns)
    cursor = db.cursor()
    cursor.execute(query)
    reponse = cursor.fetchall()
    if (reponse):
        data['city'] = reponse[0][0]
        return render_template('city.html', city=data['city'], state=find_state(data['city']), text=find_state_data(data['city']))

    
@app.route("/do_query_7")
def query_7():
    weather_type = ''
    if request.args.get('q7_weather') == 'least':
        weather_type = ' DESC'
    query = """SELECT city_name FROM 
            (SELECT H.city_name FROM HighC H INNER JOIN LowC L ON H.city_name = L.city_name ORDER BY (H.val_sum - L.val_sum) """+weather_type+""")
            WHERE ROWNUM = 1"""
    db = cx_Oracle.connect('cis450project', 'cis450project', dsn_tns)
    cursor = db.cursor()
    cursor.execute(query)
    reponse = cursor.fetchall()
    if (reponse):
        data['city'] = reponse[0][0]
        return render_template('city.html', city=data['city'], state=find_state(data['city']), text=find_state_data(data['city']))

@app.route("/weather")
def compare_weather():
    city_a = request.args.get('city1')
    city_b = request.args.get('city2')
    data['citya'] = city_a
    data['cityb'] = city_b
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT * from Precip where city_name='" + city_a + "'")
    response = cursor.fetchone()
    if not response:
        return render_template('not_found.html')
    for i in range(1,13):
        data['a'+str(i)] = response[i]
    cursor.execute("SELECT * from Precip where city_name='" + city_b + "'")
    response = cursor.fetchone()
    if not response:
        return render_template('not_found.html')
    for i in range(1,13):
        data['b'+str(i)] = response[i] 
    # Low Temp
    cursor.execute("SELECT * from LowC where city_name='" + city_a + "'")
    response = cursor.fetchone()
    for i in range(1,13):
        data['al'+str(i)] = response[i] 
    cursor.execute("SELECT * from LowC where city_name='" + city_b + "'")
    response = cursor.fetchone()
    for i in range(1,13):
        data['bl'+str(i)] = response[i] 
    # High temp
    cursor.execute("SELECT * from HighC where city_name='" + city_a + "'")
    response = cursor.fetchone()
    for i in range(1,13):
        data['ah'+str(i)] = response[i] 
        cursor.execute("SELECT * from HighC where city_name='" + city_b + "'")
    response = cursor.fetchone()
    if (response):
        for i in range(1,13):
            data['bh'+str(i)] = response[i] 
        return render_template('results.html', data=data)
    else:
        return render_template('not_found.html')

def find_state(city):
    if (city == 'Phoenix'):
        return 'Arizona'
    elif (city == 'Philadelphia'):
        return 'Pennsylvania'
    elif (city == 'Houston'):
        return 'Texas'
    elif (city == 'New York'):
        return 'New York'
    elif (city == 'Los Angeles'):
        return 'California'
    elif (city == 'Chicago'):
        return 'Illinois'
    elif (city == 'San Antonio'):
        return 'Texas'
    elif (city == 'San Diego'):
        return 'California'
    elif (city == 'Dallas'):
        return 'Texas'
    elif (city == 'San Francisco'):
        return 'California'


def find_state_data(city):
    if (city == 'Phoenix'):
        return '''Phoenix is the capital and most populous city of the U.S. state of Arizona. With 1,563,025 people (as of 2015), 
                Phoenix is the sixth most populous city nationwide, the most populous state capital in the United States, and the 
                only state capital with a population of more than one million residents.'''
    elif (city == 'Philadelphia'):
        return '''Philadelphia is the largest city in the Commonwealth of Pennsylvania and the fifth-most populous city in the 
                United States, with an estimated population of 1,567,442 and more than 6 million in the seventh-largest
                metropolitan statistical area.'''
    elif(city == 'Houston'):
        return '''Houston is the most populous city in the state of Texas and the fourth-most populous city in the United States. 
                With a census-estimated 2014 population of 2.239 million[5] within an area of 667 square miles (1,730 km2), it is 
                also the largest city in the southern United States[7] and the seat of Harris County.'''
    elif(city == 'New York'):
        return '''New York, is the most populous city in the United States. 
                New York City is also the most densely populated major city in the United States.'''
    elif(city == 'Los Angeles'):
        return '''Officially the City of Los Angeles and often known by its initials L.A., is the cultural, financial,
                and commercial center of Southern California. With a census-estimated 2015 population of 3,971,883, 
                it is the second-most populous city in the United States (after New York City) and the most populous city in California.'''
    elif(city == 'Chicago'):
        return '''Officially the City of Chicago, is the third-most populous city in the United States.
                In terms of wealth and economy, Chicago is considered one of the most important business centers in the world.'''
    elif(city == 'San Antonio'):
        return '''Officially the City of San Antonio, is the seventh-most populated city in the United States and the 
                second-most populous city in the state of Texas, with a population of 1,409,019.[5] It was the fastest 
                growing of the top 10 largest cities in the United States from 2000 to 2010, and the second from 1990 to 2000.'''
    elif(city == 'San Diego'):
        return '''San Diego is a major city in California, United States. It is in San Diego County, on the coast of the Pacific Ocean
                in Southern California, approximately 120 miles (190 km) south of Los Angeles and immediately adjacent to the border 
                with Mexico. With an estimated population of 1,394,928 as of July 1, 2015, San Diego is the eighth-largest city in 
                the United States and second-largest in California.'''
    elif(city == 'Dallas'):
        return '''Dallas is the most populous city in the Dallas Fort Worth metroplex, 
                the fourth most populous metropolitan area in the United States. 
                The city's population ranks ninth in the U.S. and third in Texas after Houston and San Antonio.'''
    elif(city == 'San Francisco'):
        return '''Officially the City and County of San Francisco,  
                is the cultural, commercial, and financial center of Northern California.
                It is the birthplace of the United Nations.'''
