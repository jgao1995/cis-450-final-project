
With new_table AS (SELECT city_name, price
FROM Zillow H
WHERE price > (SELECT AVG(Z.price) FROM Zillow Z)) SELECT new_table.city_name FROM 
Precip P INNER JOIN new_table ON P.city_name = new_table.city_name 
WHERE P.val_sum < (SELECT AVG(Pr.val_sum) FROM Precip Pr) ORDER BY P.val_sum;