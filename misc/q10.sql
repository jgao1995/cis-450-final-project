SELECT H.city_name
FROM HighC H INNER JOIN LowC L ON H.city_name = L.city_name
WHERE rownum < 2
ORDER BY H.val_sum - L.val_sum;
