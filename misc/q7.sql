
SELECT H.zip
FROM Zillow H
WHERE H.price > (SELECT avg(price)-100 FROM Zillow) AND
H.price < (SELECT avg(price)+100 FROM Zillow);