WITH new_table AS (SELECT * FROM Zillow Z INNER JOIN highC W ON 
  (Z.city_name = W.city_name OR Z.county_name = W.city_name OR Z.metro_name = W.city_name)
  WHERE W.val_sum >= 21 * 12) SELECT zi.city_name from Zillow zi WHERE 
  zi.price = (SELECT MIN (new_table.price) FROM new_table);


shorter version:
select city_name
from zillow
where price =
	(select min(price)
	from zillow z inner join highc c on z.city_name = c.city_name
	where c.val_sum > 21*12);