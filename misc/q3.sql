SELECT H.zip
FROM Zillow H INNER JOIN Precip R on (H.city_name = R.city_name OR H.county_name = R.city_name OR
H.metro_name = R.city_name) WHERE (R.val_sum) <= (SELECT min(val_sum) FROM Precip)
AND R.city_name = H.city_name
AND H.price = (SELECT MIN(Z.price)
FROM Zillow Z
WHERE Z.city_name = H.city_name);