from yelp.client import Client
from yelp.oauth1_authenticator import Oauth1Authenticator
from IPython import embed
import json
import os

auth = Oauth1Authenticator(
    consumer_key='jRMCq9bemx4CJS6WiMj8LQ',
    consumer_secret='2pYLc30kpPwIZn2UR6cWhdwce28',
    token='8d4ZzLX4Yxnwf0ncJLZ86yJ4MSIUP-Ol',
    token_secret='Orr7niz1vd2BoV-KUMkwz_eE8Ec'
)

# term should be something like 'food' or 'doctors'
# loc should be of the form 'San Francisco' or 'Chicago'
def query(term, loc):
    filename = loc.replace(" ", "") + term + '.json'
    client = Client(auth)
    try:
        os.remove(filename)
    except OSError:
        pass
    all_dicts = []
    for i in range(0, 50):
        params = {
                'term': term,
                'sort': 1,
                'offset': i
        }
        response = client.search(loc, **params)
        dicts_to_output = [
            {
                'name': biz.name,
                'id': biz.id,
                'city': biz.location.city,
                'state': biz.location.state_code,
                'postal_code': biz.location.postal_code,
                'neighborhood': biz.location.neighborhoods,
                'top_category': biz.categories[0].alias if biz.categories else '',
                'rating': biz.rating,
                'review_count': biz.review_count
            }
            for biz in response.businesses
        ]
        all_dicts.extend(dicts_to_output)
    with open(filename, 'a') as fp:
        json.dump(all_dicts, fp, indent=4)

