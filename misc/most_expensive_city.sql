SELECT city_name 
FROM Zillow
GROUP BY city_name
ORDER BY avg(price) DESC;