SELECT P.city_name
FROM Precip P
WHERE P.val_sum = (SELECT MIN(Pr.val_sum) FROM Precip Pr);